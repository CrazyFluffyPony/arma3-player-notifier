
const Gamedig = require('gamedig');

module.exports = (() => {

    function getData(ip, port){

        /*

        Server Summary
        Name: [AhoyWorld.net] EU#1 CO-OP Dedicated Invade & Annex 3.3.34 [Aho
        Game: ARMA 3   Browse: ARMA 3 Servers
        Address: 144.76.82.169   Port: 2302   Status: Alive
        Server Manager: kamaradski (claim server)


        https://github.com/sonicsnes/node-gamedig

        */

        return new Promise((fulfill, reject)=>{
            const A = {/* dummy object for testing */
                name: "AhoyWorld",
                map: "Agis",
                password: true,
                maxplayers: 10,
                players: [
                    {},
                    {},
                    {
                        name: "Peter",
                        ping: 299,
                        score: 50,
                        team: "maunz",
                        address: "i dont know"
                    },
                    {
                        name: "Paul",
                        ping: 99
                    },
                    {
                        name: "Mary"
                    }
                ],
                bots: [
                    {}
                ],
                connect: "wuff.gameserver.de:1234",
                ping: 199,
                raw: {}
            }
            const B = {/* dummy object for testing */
                name: "AhoyWorld",
                map: "Agis",
                password: true,
                maxplayers: 10,
                players: [
                    {},
                    {
                        name: "Peter",
                        ping: 299,
                        score: 50,
                        team: "maunz",
                        address: "i dont know"
                    },
                    {
                        name: "Mary"
                    }
                ],
                bots: [
                    {}
                ],
                connect: "wuff.gameserver.de:1234",
                ping: 199,
                raw: {}
            }

            const C = {/* dummy object for testing */
                name: "AhoyWorld",
                map: "Agis",
                password: true,
                maxplayers: 10,
                players: [
                    {},
                    {
                        name: "Mary"
                    }
                ],
                bots: [
                    {}
                ],
                connect: "wuff.gameserver.de:1234",
                ping: 199,
                raw: {}
            }


            /*const rand = Math.random()
            if(rand > 0.3 && rand < 0.6 ){        
                fulfill(A)
            } else if (rand > 0.6){
                fulfill(B)
            } else {
                fulfill(C)
            }*/

            Gamedig.query({
                type: 'arma3',
                host: ip,
                port: port
            }).then((state) => {
                console.log(state)
                fulfill(state)
            }).catch((error) => {
                console.log("Server is offline", error)
                reject(error)
            });
        })
    }



    return {
        getData: getData
    }
})()