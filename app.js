var express = require('express')
var path = require('path')
var logger = require('morgan')
var bodyParser = require('body-parser')
var cors = require('cors')
var express_json = require('express-json')

const database_uri = "postgres://ponybin:89a751lsdfoRAD13sd8@localhost:5432/ponybin"


var handlebars = require('express-handlebars') /* renderer for express */

ponybin = require('./modules/ponybin.js')


/* Database */
/* routes */
var index = require('./routes/index')
var api = require('./routes/api')

var app = express();

var nosniff = require('dont-sniff-mimetype')
app.use(nosniff())


/* security */
app.disable('x-powered-by');//dont tell client that i use express
//http://expressjs.com/de/advanced/best-practice-security.html#helmet-verwenden
var helmet = require('helmet');
app.use(helmet());

var hsts = require('hsts')
app.use(hsts({
  maxAge: 15552000  // 180 days in seconds
}))


// Register '.handlebars' extension with Handlebars
app.engine('handlebars', handlebars({defaultLayout: 'default'}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');



app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express_json())



/* load routes */

function dontCacheThisRoute(req, res, next){
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
  res.setHeader('Pragma', 'no-cache')
  res.setHeader('Expires', 0)
  next()
}

app.use(cors())

app.use(express.static('public'))


app.use('/', index)
app.use('/api', dontCacheThisRoute)
app.use('/api', api)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found: ' + req.url);
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  console.error('rendering error page:', err)
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
