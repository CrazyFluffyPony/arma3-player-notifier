var express = require('express');
var router = express.Router();



router.post('/data', async function(req, res, next) {
    console.log('/api/data POST', req.body)

    if(req.body.address && req.body.port){
        ponybin.getData(req.body.address, req.body.port).then((state)=>{
            res.send(state)
        }).catch((err)=>{
            error(res, "Cannot get data")
        })
    } else {
        error(res, 'Invalid ip or port')
    }
});

function error(res, message){
    res.status(400)
    res.send({
        'status': 'error',
        'message': message
    })
}


module.exports = router;
