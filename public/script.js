var apn = (($)=>{
    "use strict";

    $(window).on('load', ()=>{
        init()
    })

    function init(){
        $('body .notifier_container').each((index, elem)=>{
            new Notifier(elem)
        })
    }

    return {

    }
})(jQuery)


class Notifier {

    constructor(container){
        this.refreshTime = 10000 // = 8640 requests pro Tag
        this.data = {players: []}

        this.playersWatched = []

        this.dom = $(container)

        $(this.dom).find('.notifier_connect').on('click', ()=>{
            this.connect()
        })
        this.dom.find('.notifier_view').hide()
    }

    connect(){
        let address = this.dom.find('.notifier_server_ip').val()
        let port = this.dom.find('.notifier_server_port').val()

        if(!address || !port){
            this.dom.find('.error').show().find('.message').html('Address or Port invalid')
            setTimeout(()=>{
                this.dom.find('.error').hide()
            }, 3000)

            return
        }

        //else sucessfull
        this.dom.find('.notifier_setup').hide()
        this.dom.find('.notifier_view').show()

        this.address = address
        this.port = port

        this.refreshInterval = setInterval(()=>{
            this.refresh()
        }, this.refreshTime)
        this.refresh()
    }

    refresh(){
        $.post('api/data', {address: this.address, port: this.port})
            .done((data)=>{            
                try {
                    let parsed = JSON.parse(data)
                    data = parsed
                    

                    if(data.players.length != this.data.players.length){
                        let diff = this.data.players.length - data.players.length
                        console.log('number of players changed', diff < 0 ? -diff + ' players joined the server' : diff + ' players left the server')
                    }

                    let playersLeft = []

                    for(let p of this.data.players){
                        if(p.name){
                            let existsInNewDataSet = false
                            for(let pnew of data.players){
                                if(p.name === pnew.name){
                                    existsInNewDataSet = true
                                    break
                                }
                            }

                            if(!existsInNewDataSet){
                                console.log('player', p.name, 'left the server')
                                for(let i in this.playersWatched){
                                    let pw = this.playersWatched[i]
                                    if(pw.name === p.name){
                                        console.log('watched player ' + pw.name + ' left the server')
                                        playersLeft.push(pw)
                                        this.unwatch(pw)
                                        break
                                    }
                                }
                            }
                        } else {
                            console.log('skipping player without a name')
                        }
                    }
                    this.data = data

                    this.updateDom()
                    this.dom.find('.refresh_fail').hide()


                    this.dom.find('.players_left').html('')
                    for(let pl of playersLeft){
                        this.dom.find('.players_left').append('<span>' + (pl.name || 'unknown') + '</span>')
                    }
                    if(playersLeft.length > 0){
                        this.dom.find('.players_left_container').show()
                        this.dom.find('.players_left').append('<span class="left">left the server</span>')
                    }
                    setTimeout(()=>{
                        this.dom.find('.players_left_container').hide()
                    }, 5000)
                } catch (e){
                    console.error('error parsing api data', e)
                    this.dom.find('.refresh_fail').show()                    
                }
            })
            .fail((err)=>{
                console.error('error fetching api/data', err)
                this.dom.find('.refresh_fail').show()
            })
    }

    updateDom(){
        this.dom.find('.server_name').html(this.data.name)
        this.dom.find('.player_list').html('')
        for(let p of this.data.players){
            let watched = false
            for(let pw of this.playersWatched){
                if (pw.name === p.name) {
                    watched = true
                    break
                }
            }

            if(watched){
                let it = $('<div class="player watched">' + (p.name || 'unknown') + '<span class="unwatch icon-eye-blocked"></span></div>')
                this.dom.find('.player_list').append(it)
                it.find('.unwatch').on('click', ()=>{
                    this.unwatch(p)
                })
            } else {
                let it = $('<div class="player">' + (p.name || 'unknown') + '<span class="watch icon-eye"></span></div>')
                this.dom.find('.player_list').append(it)
                it.find('.watch').on('click', ()=>{
                    this.watch(p)
                })
            }
        }

        this.dom.find('.player_count').html(this.data.players && this.data.players instanceof Array ? this.data.players.length + ' Players' : '? Players')

        console.log('currently watched players:', this.playersWatched)
    }

    watch(player){
        console.log('watching player ' + player.name)
        for(let i in this.playersWatched){
            let pw = this.playersWatched[i]
            if(pw.name === player.name){
                console.log('player ' + player.name + 'is already watched')
                this.updateDom()
                return
            }
        }
        this.playersWatched.push(player)
        this.updateDom()
    }

    unwatch(player){
        console.log('unwatching player ' + player.name)
        for(let i in this.playersWatched){
            let pw = this.playersWatched[i]
            if(pw.name === player.name){
                this.playersWatched.splice(i, 1)
                this.updateDom()
                return
            }
        }
        console.log('player ' + player.name + 'is already unwatched')
        this.updateDom()
    }
}